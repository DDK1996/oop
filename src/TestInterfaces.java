public class TestInterfaces {
    public static void main(String[] args) {
        Detailable[] detailables= { new HomeInsurance(420,35,1000),
                                    new SavingsAccount("Dean",9),
                                    new CurrentAccount("Tim",7)};

        for (Detailable object: detailables
             ) {
            System.out.println(object.getDetails()+"\n");
        }

    }
}

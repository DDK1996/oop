public class HelloWorld {
    public static void main(String[] args) {
        int normalint=2;

        short firstShort=3;
        short secondShort= (short) (firstShort+2);

        double firstDouble=1.2;

        char myFirstChar='a';
        String myFirstString="This is a string";

        String make = "Renault";
        String model = "Laguna";
        double engineSize = 1.8;
        byte gear = 2;
        System.out.println("The make is " + make);
        System.out.println("The model is"  + model);
        System.out.println("The engine size is " + engineSize);

        String example ="example.doc";

        String fixed=example.replace("o","a");
        String finished=fixed.replace("c","k");
        String fin= finished.replace("d","b");
        System.out.println(normalint++);
    }
}
